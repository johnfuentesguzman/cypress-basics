# README #

Understating with this dummy project how cypress works for making automation test

### What Will I learn? ###

* Installation --> https://docs.cypress.io/guides/getting-started/installing-cypress#npm-install


* Running cypress testing env:

Using Cypress UI : (npm bin)/cypress open

![cypress testing env](repo/images/cypress testing enviroment.png "cypress testing env") 

Using commdand line/ console for running test : npx cypress run  

Using commdand line/ console for running test THROUGH ELECTRON BROWSER  : npx cypress run --headed

Using commdand line/ console for running test THROUGH CHROME BROWSER  : npx cypress run --browser chrome 


Running a Specific test case byy console/terminal: npx cypress run --spec "relative path oh the test file" 

* Cypress Project Folder Structure:

![Project Folder Structure](repo/images/ Cypress Project Folder Structure.png "Project Folder Structure") 

* Locating/Getting and interacting with the elements of the UI by cypress : 

test file: cypress/integration/examples/handlingUIElements.spec.js

command for runnig it: npx cypress run --spec "cypress/integration/examples/handlingUIElements.spec.js" 